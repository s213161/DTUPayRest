package org.acme;


import bank.entities.Account;
import dtu.ws.fastmoney.*;
import errorMessages.ErrorType;
import models.CreateAccountData;
import services.AccountService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/accounts")
public class AccountController {
    AccountService accountService = new AccountService();
    BankService bankService = new BankServiceService().getBankServicePort();


    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getAccounts() {
        //get all the accounts from DTU PAY
        return Response.status(Response.Status.OK).entity(accountService.getAccountsInfo()).build();
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response postAccount(CreateAccountData createAccountData) {
        //Create an account in DTU PAY
        try{
            Account account = accountService.postCreateAccount(createAccountData);
            return Response.status(Response.Status.CREATED)
                    .entity("Account Created id {" + account.getAccountId() + "}").build();
        } catch (Exception e){
            //validation catch
            return Response.status(Response.Status.BAD_REQUEST).entity("Please check the Json payload. " + e).build();
        }
    }


    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getAccount(@PathParam("id") String id) {
        //get account with provided id
        Account accountToReturn;
        try{
            accountToReturn = accountService.getAccountInfo(id);

            if(accountToReturn != null){
                return Response.status(Response.Status.OK).entity(accountToReturn).build();
            }
            else {
                ErrorType errorType = new ErrorType("Account with id: " + id + " not found");
                return Response.status(Response.Status.NOT_FOUND).entity(errorType) .build();
            }

        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST).entity(e) .build();
        }
    }


    @DELETE
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response deleteAccount(@PathParam("id") String id) {
        try{
            if (accountService.deleteAccountInfo(id)){
                return Response.status(Response.Status.NO_CONTENT).entity("Account with id: " + id + " deleted").build();
            }
            else {
                return Response.status(Response.Status.NOT_FOUND).entity("Account does not exist!").build();
            }
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST).entity("Something went wrong, " + e ).build();
        }
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response deleteAllAccount() {
        accountService.deleteAllAccounts();
        return Response.status(Response.Status.OK).build();
    }

}
