package org.acme;


import models.PaymentData;
import services.PaymentService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/payment")
public class PaymentController {

    PaymentService paymentService = new PaymentService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createPayment(PaymentData paymentData) {

        try{
            if (paymentService.postCreatePayment(paymentData)){
                return Response.status(Response.Status.NO_CONTENT).entity("OK").build();
            }
            else{
                return Response.status(Response.Status.FORBIDDEN).entity("Debit or Credit client not found").build();
            }
        }catch (Exception e){
            System.out.println("");
            return Response.status(Response.Status.BAD_REQUEST).entity(e).build();
        }
    }
}
