package bank.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "user")
@Data // Automatic getter and setters and equals etc
@NoArgsConstructor // Needed for JSON deserialization and XML serialization and deserialization
@AllArgsConstructor
public class User {
    private String cprNumber;
    private String firstName;
    private String lastName;

    public boolean isLegal(){
        if(cprNumber==null){
            return false;
        }
        if(firstName==null){
            return false;
        }
        if(lastName==null){
            return false;
        }
        return true;
    }
}
