package bank.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;


@XmlRootElement(name = "transaction")
@Data // Automatic getter and setters and equals etc
@NoArgsConstructor // Needed for JSON deserialization and XML serialization and deserialization
@AllArgsConstructor
public class Transaction {
    private int transactionId;
    private double amount;
    private double balance;
    private String creditor;
    private String debtor;
    private String description;
    private LocalDateTime time ;
}
