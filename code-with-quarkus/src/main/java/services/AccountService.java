package services;

import bank.entities.Account;
import bank.entities.Transaction;
import bank.entities.User;
import errorMessages.ErrorType;
import models.AccountInfo;
import models.CreateAccountData;
import java.util.*;
import java.util.stream.Collectors;

public class AccountService {

//    private static ArrayList<Account> accounts = new ArrayList<>();
    private static Integer accountId = 0;
    private static User user1 = new User("cpr12", "hubert", "dtu");
    private static User user2 = new User("cpr13", "adam", "dtu");
    public static ArrayList<Account> accounts = new ArrayList<Account>() {
        {
            add(new Account("123", 100, user1, new ArrayList<>()));
            add(new Account("124", 150, user2, new ArrayList<>()));
        }
    };

    public List<AccountInfo> getAccountsInfo(){
        return accounts.stream().map(x-> new AccountInfo(x.getAccountId(), x.getUser()))
                .collect(Collectors.toList());
    }

    public Account postCreateAccount(CreateAccountData createAccountData) throws Exception {
        ErrorType errorType = new ErrorType();

        //validation of JSON payload
        if(createAccountData.getUser()==null){
            errorType.setErrorMessage("User data not provided.");
            throw new Exception(errorType.getErrorMessage());
        }
        if(!createAccountData.getUser().isLegal()){
            errorType.setErrorMessage("User data wrong format.");
            throw new Exception(errorType.getErrorMessage());
        }
        if(createAccountData.getBalance()<0){
            errorType.setErrorMessage("Balance cannot be negative.");
            throw new Exception(errorType.getErrorMessage());
        }

        Account account = new Account((accountId++).toString(), createAccountData.getBalance(),
                createAccountData.getUser(), new ArrayList<Transaction>());

        accounts.add(account);

        return account;
    }

    public Account getAccountInfo(String accountId) throws Exception {
        //get one account with provided ID
        ErrorType errorType = new ErrorType();

        if(accountId ==  null){
            errorType.setErrorMessage("Account id not provided!");
            throw new Exception(errorType.getErrorMessage());
        }

        Optional<Account> accountToReturn = accounts.stream().
                filter((elm) -> elm.getAccountId().equals(accountId) ).findFirst();

        return (Account) accountToReturn.orElse(null);
    }

    public boolean deleteAccountInfo(String accountId) throws Exception {
        //delete account with provided ID
        ErrorType errorType = new ErrorType();
        boolean foundIDAndDeleted = false;

        if(accountId ==  null){
            errorType.setErrorMessage("Account id not provided!");
            throw new Exception(errorType.getErrorMessage());
        }

//        Optional<Account> accountToReturn = accounts.stream().
//                filter((elm) -> elm.getAccountId().equals(accountId) ).findFirst();

        // Creating iterator object
        Iterator<Account> itr = accounts.iterator();
        while (itr.hasNext()) {
            Account x = (Account)itr.next();
            if (x.getAccountId().equals(accountId)) {
                itr.remove();
                foundIDAndDeleted = true;
            }
        }
        return foundIDAndDeleted;
    }

    public void deleteAllAccounts(){
        // Creating iterator object
        Iterator<Account> itr = accounts.iterator();
        while (itr.hasNext()) {
            Account x = (Account)itr.next();
                itr.remove();
        }
    }
}
