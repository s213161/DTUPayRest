package services;


import bank.entities.Account;
import bank.entities.Transaction;
import bank.entities.User;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import errorMessages.ErrorType;
import models.PaymentData;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;


public class PaymentService {
    private static Integer transactionId = 0;
    private static User user = new User("cpr12", "hubert", "dtu");
    BankService bankService = new BankServiceService().getBankServicePort();

    public boolean postCreatePayment(PaymentData paymentData) throws Exception {
        ErrorType errorType = new ErrorType();
        Account accountCredit = null;
        Account accountDebit = null;

        if(paymentData == null){
            errorType.setErrorMessage("Wrong Json.");
            throw new Exception(errorType.getErrorMessage());
        }

        //Contact bank to do payment
        try {
            bankService.transferMoneyFromTo(paymentData.getCreditor(), paymentData.getDebtor(), BigDecimal.valueOf(paymentData.getAmount()), paymentData.getDescription());
        }
        catch (BankServiceException_Exception e) {
           return false;
        }

        //DTUPay
//        for(Account account: AccountService.accounts){
//            if(account.getUser().getCprNumber().equals(paymentData.getDebtor())){
//                accountDebit = account;
//            }
//            if(account.getUser().getCprNumber().equals(paymentData.getCreditor())){
//                accountCredit = account;
//            }
//        }
//
//        if(accountCredit == null || accountDebit == null){
//            return false;
//        }
//
//        Transaction transactionDebit = new Transaction(transactionId++, paymentData.getAmount(),
//                accountDebit.getBalance() + paymentData.getAmount(),
//                paymentData.getCreditor(), paymentData.getDebtor(), paymentData.getDescription(), LocalDateTime.now());
//
//        ArrayList<Transaction> transactions = accountDebit.getTransactions();
//        transactions.add(transactionDebit);
//        accountDebit.setTransactions(transactions);
//        accountDebit.setBalance(accountDebit.getBalance() + paymentData.getAmount());
//
//        Transaction transactionCredit = new Transaction(transactionId++, paymentData.getAmount(),
//                accountCredit.getBalance() - paymentData.getAmount(),
//                paymentData.getCreditor(), paymentData.getDebtor(), paymentData.getDescription(), LocalDateTime.now());
//
//        transactions = accountCredit.getTransactions();
//        transactions.add(transactionCredit);
//        accountCredit.setTransactions(transactions);
//        accountCredit.setBalance(accountCredit.getBalance() - paymentData.getAmount());

        return true;
    }
}
