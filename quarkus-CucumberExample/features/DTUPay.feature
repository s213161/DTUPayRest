Feature: Accounts
	Scenario: When i try to get accountinfo with id i get it
		When I call the account service to get account with id "123" son via JSON
		Then I get an account with id "123"

	Scenario: When I try to get accountinfo with id I get 200 status
		When I call the account service to get account with id "123"
		Then I get result "200"

	Scenario: Account not found
		When I call the account service to get account with id "121-2" that does not exist
		Then I get result "404"

	Scenario: When i try to get accountinfolist i get it
		When I call the account service to get a list of all accountInfo via JSON
		Then I get a List of account with length 2

	Scenario: When I try to get accountinfolist I get 200 status
		When I call the account service to get a list of all accountInfo
		Then I get result "200"

	Scenario: I can create an account
		When I Post a new account with balance 5 and user with cpr "testcpr" name "TestName" and lastname "testLastName"
		Then I get result "201"
		And I created the account

	Scenario: Do not allow creating an account with negative balance
		When I Post a new account with negative balance -1 and user with cpr "cpr000" name "namename" and lastname "lastName1"
		Then I get result "400"

#	Scenario: Bank account does not exist when we create a DTUPay account ?
	#need to ask a TA for that if we are suppose to consume the API in dtu pay

	Scenario: Delete Account
		Given a user has a DTUPay account
		When I delete the account
		Then the account does not exist any more

	Scenario: Error when I try to delete an account that does not exist
		When I delete an account with id "987" that does not exist
		Then I get result "404"

#	Scenario: Create a DTUPay payment
#	When A creditor with cpr number "cpr12" pays 10 kr to a debtor with cpr number "cpr13"
#	Then I get result "204"
#
#	Scenario: Try to create a DTUPay payment with non existing account
#		When A non existing creditor with cpr number "cpr12121212" pays 10 kr to a debtor with cpr number "cpr13"
#		Then I get result "400"

