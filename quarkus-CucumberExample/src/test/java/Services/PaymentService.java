package Services;

import JSONEntities.dtuPay.PaymentData;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class PaymentService extends ServiceBase{

    public Response doPayment(PaymentData paymentData){
        return baseUrl.path("payment")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(paymentData, MediaType.APPLICATION_JSON));
    }
}
