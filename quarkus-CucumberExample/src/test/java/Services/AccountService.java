package Services;

import JSONEntities.dtuPay.AccountInfo;
import JSONEntities.dtuPay.AccountInfoList;
import JSONEntities.dtuPay.CreateAccountData;
import JSONEntities.dtuPay.PaymentData;

import javax.json.JsonArray;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccountService extends ServiceBase {

    public AccountInfo getAccountInfoJson(String id) {
        return getAccountInfo(id, MediaType.APPLICATION_JSON);
    }

    public AccountInfoList getAccountInfoListJson() {return getAccountInfoList(MediaType.APPLICATION_JSON);}

    public Response setAccountFromJson(CreateAccountData info) {
        return baseUrl.path("accounts")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(info, MediaType.APPLICATION_JSON));
    }

    public String getAccountIdFromResponse(Response response) {
        String retVal = "";
        String responseEntity =  response.readEntity(String.class);

        Pattern logEntry = Pattern.compile("\\{(.*?)\\}");
        Matcher x = logEntry.matcher(responseEntity);
        if(x.find()) {
            retVal = x.group(1);
        }

        return retVal;
    }

    public AccountInfo getAccountInfo(String id, String mediaType) {
        return baseUrl.path("accounts")
                .path(id)
                .request()
                .accept(mediaType)
                .get(AccountInfo.class);
    }

    public Response getAccountResponse(String id) {
        return baseUrl.path("accounts")
                .path(id)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

    public AccountInfoList getAccountInfoList(String mediaType) {
        var list = baseUrl.path("accounts")
                .request()
                .accept(mediaType)
                .get(JsonArray.class)
                .getValuesAs(jsonValue -> {
                    AccountInfo account = new AccountInfo();
                    account.setAccountId(jsonValue.asJsonObject().getString("accountId"));
                    return account;
                });

        AccountInfoList accountInfoList = new AccountInfoList();
        accountInfoList.accountInfoList = list;

        return accountInfoList;
    }

    public Response getAccountInfoListResponse() {
        return baseUrl.path("accounts")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

//    public Response postPaymentFromJson(PaymentData data) {
//        return baseUrl.path("accounts")
//                .request(MediaType.TEXT_PLAIN)
//                .post(Entity.entity(data, MediaType.APPLICATION_JSON));
//    }

    public Response deleteAccount(String id) {
        return baseUrl.path("accounts")
                .path(id)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .delete();
    }

    public Response deleteAllAccount() {
        return baseUrl.path("accounts")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .delete();
    }
}
