package dtu.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import JSONEntities.dtuPay.*;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import Services.AccountService;
import Services.PaymentService;
import io.cucumber.java.AfterAll;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import java.math.BigDecimal;


public class ExampleSteps {
	Client client = ClientBuilder.newClient();
	String targetURL = "http://localhost:8080";
	static AccountService accountService = new AccountService();
	AccountInfo account;
	AccountInfoList accountList;
	static BankService bankService = new BankServiceService().getBankServicePort();
	PaymentService paymentService = new PaymentService();

	private static String bankAccountIdCustomer = null;
	private static String bankAccountIdMerchant = null;
	private boolean successfulTransfer = false;

	private static String accountToBeDeletedId;
	private static String newlyCreatedAccountId;
	private static String customerAccountId;
	private static String merchantAccountId;

	private Response response;
	private Response.Status status;

//	GET Account
	@When("I call the account service to get account with id {string} son via JSON")
	public void iCallTheAccountServiceToGetAccountInfoViaJSON(String id) {
		account = accountService.getAccountInfoJson(id);
	}

	@When("I call the account service to get account with id {string}")
	public void iCallTheAccountServiceToGetAccountWithId(String id) {
		response = accountService.getAccountResponse(id);
	}



	@When("I call the account service to get account with id {string} that does not exist")
	public void iCallTheAccountServiceToGetAccountWithIdThatDoesNotExist(String id) {
		response = accountService.getAccountResponse(id);
	}

//	GET Accounts
	@When("I call the account service to get a list of all accountInfo via JSON")
	public void iCallTheAccountServiceToGetAccountInfoViaJSON() {
		accountList = accountService.getAccountInfoListJson();
	}

	@When("I call the account service to get a list of all accountInfo")
	public void iCallTheAccountServiceToGetAListOfAllAccountInfo() {
		response = accountService.getAccountInfoListResponse();
	}

//	POST Account
	@When("I Post a new account with balance {double} and user with cpr {string} name {string} and lastname {string}")
	public void iCallTheAccountServiceToCreateNewAccount(double balance, String cpr, String name, String lastNAme) {
		CreateAccountData AD = new CreateAccountData();
		AD.setBalance(balance);
		AD.setUser(new User(cpr, name, lastNAme));

		response = accountService.setAccountFromJson(AD);
		newlyCreatedAccountId = accountService.getAccountIdFromResponse(response);
	}

	@When("I Post a new account with negative balance {double} and user with cpr {string} name {string} and lastname {string}")
	public void iPostANewAccountWithNegativeBalanceAndUserWithCprNameAndLastname(double balance, String cpr, String name, String lastName) {
		CreateAccountData AD = new CreateAccountData();
		AD.setBalance(balance);
		AD.setUser(new User(cpr, name, lastName));

		response = accountService.setAccountFromJson(AD);
	}

	@Then("I get result {string}")
	public void iGetResult(String id) {
		Assertions.assertEquals(id, String.valueOf(response.getStatus()));
	}

	@And("I created the account")
	public void iCreatedTheAccount() {
			assertEquals(accountService.getAccountResponse(newlyCreatedAccountId).getStatusInfo().toEnum(), Response.Status.OK);
	}

	@Then("I get an account with id {string}")
	public void iGetAnAccountInfoWithId(String id) {
		Assertions.assertEquals(id, account.getAccountId());
	}

	@Then("I get a List of account with length {int}")
	public void iGetAnAccountInfoListWithLength(int length) {
		Assertions.assertEquals(length, accountList.accountInfoList.size());
	}

	// POST Payment
	@When("A creditor with cpr number {string} pays {double} kr to a debtor with cpr number {string}")
	public void aCreditorWithCprNumberPaysKrToADebtorWithCprNumber(String creditor, double amount , String debtor) {
		PaymentData data = new PaymentData();
		data.setCreditor(creditor);
		data.setDebtor(debtor);
		data.setAmount(amount);
		data.setDescription("TEST");

		response = paymentService.doPayment(data);
	}

	@When("A non existing creditor with cpr number {string} pays {int} kr to a debtor with cpr number {string}")
	public void aNonExistingCreditorWithCprNumberPaysKrToADebtorWithCprNumber(String creditor, int amount, String debtor) {
		PaymentData data = new PaymentData();
		data.setCreditor(creditor);
		data.setDebtor(debtor);
		data.setAmount(amount);
		data.setDescription("TEST-2");

		response = paymentService.doPayment(data);
	}

	// DELETE account
	@Given("a user has a DTUPay account")
	public void aUserHasADTUPayAccount() {
		String cpr = "mel123";
		String name = "melmel";
		String lastName = "verver";

		CreateAccountData AD = new CreateAccountData();
		AD.setUser(new User(cpr, name, lastName));

		response = accountService.setAccountFromJson(AD);
		accountToBeDeletedId = accountService.getAccountIdFromResponse(response);
	}

	@When("I delete the account")
	public void iDeleteTheAccount() {
		accountService.deleteAccount(accountToBeDeletedId);
	}

	@When("I delete an account with id {string} that does not exist")
	public void iDeleteAnAccountWithIdThatDoesNotExist(String id) {
		response = accountService.deleteAccount(id);
	}

	@Then("the account does not exist any more")
	public void theAccountDoesNotExistAnyMore() {
		assertEquals(accountService.getAccountResponse(accountToBeDeletedId).getStatusInfo().toEnum(), Response.Status.NOT_FOUND);
	}

	//SOAP testing
	@Given("a customer with a bank account with balance {int}")
	public void aCustomerWithABankAccountWithBalance(int balanceAmount) throws BankServiceException_Exception {
		dtu.ws.fastmoney.User userBank = new dtu.ws.fastmoney.User();
		userBank.setCprNumber("000000-cpr");
		userBank.setFirstName("melmel");
		userBank.setLastName("verver");
		bankAccountIdCustomer = bankService.createAccountWithBalance(userBank, BigDecimal.valueOf(balanceAmount));
	}

	@And("that the customer is registered with DTU Pay")
	public void thatTheCustomerIsRegisteredWithDTUPay() {
		User userDTUPay = new User("000000-cpr", "melmel", "verver");
		response = accountService.setAccountFromJson(new CreateAccountData(1000, userDTUPay));
		customerAccountId = accountService.getAccountIdFromResponse(response);
	}

	@Given("a merchant with a bank account with balance {int}")
	public void aMerchantWithABankAccountWithBalance(int balanceAmount) throws BankServiceException_Exception {
		dtu.ws.fastmoney.User userBank = new dtu.ws.fastmoney.User();
		userBank.setCprNumber("000001-cpr");
		userBank.setFirstName("jacjac");
		userBank.setLastName("niknik");
		bankAccountIdMerchant = bankService.createAccountWithBalance(userBank, BigDecimal.valueOf(balanceAmount));
	}

	@And("that the merchant is registered with DTU Pay")
	public void thatTheMerchantIsRegisteredWithDTUPay() {
		User userDTUPay = new User("000001-cpr", "jacjca", "niknik");
		response = accountService.setAccountFromJson(new CreateAccountData(2000, userDTUPay));
		merchantAccountId = accountService.getAccountIdFromResponse(response);
	}

	@When("the merchant initiates a payment for {int} kr by the customer")
	public void theMerchantInitiatesAPaymentForKrByTheCustomer(int amount) throws BankServiceException_Exception {
		try{
			PaymentData paymentData = new PaymentData(amount, bankAccountIdCustomer, bankAccountIdMerchant, "Show me the money!");
			paymentService.doPayment(paymentData);
			successfulTransfer = true;
		}
		catch (Exception e){
			successfulTransfer = false;
		}
	}

	@Then("the payment is successful")
	public void thePaymentIsSuccessful() {
		assertTrue(successfulTransfer);
	}

	@And("the balance of the customer at the bank is {double} kr")
	public void theBalanceOfTheCustomerAtTheBankIsKr(double balance) throws BankServiceException_Exception {
		Account customerAccount = bankService.getAccount(bankAccountIdCustomer);
		assertEquals(customerAccount.getBalance(), BigDecimal.valueOf(balance));
	}

	@And("the balance of the merchant at the bank is {double} kr")
	public void theBalanceOfTheMerchantAtTheBankIsKr(double balance) throws BankServiceException_Exception {
		Account merchantAccount = bankService.getAccount(bankAccountIdMerchant);
		assertEquals(merchantAccount.getBalance(), BigDecimal.valueOf(balance));
	}

	@AfterAll
	public static void after_all() {

//		accountService.deleteAccount(newlyCreatedAccountId);

		accountService.deleteAccount(customerAccountId);
		accountService.deleteAccount(merchantAccountId);

		try{
			bankService.retireAccount(bankAccountIdMerchant);
			bankService.retireAccount(bankAccountIdCustomer);
		}
		catch (BankServiceException_Exception e){
			System.out.println(e);
		}
	}


}

